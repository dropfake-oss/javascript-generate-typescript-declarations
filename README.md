# javascript-generate-typescript-declarations

DotNet code that generates typescript declaration files.  This works with javascript-jint.

javascript-unity can use this package to generate typescript declarations from .NET code.

This package is also published to nuget.

## Subscript/Index Access

Using "[]" to index into an instnace (like IReadOnlyList<> or List<>) will work - but there may be other similar types that need to be fixed so that we generate the subscript/index operator into that typescript declaration.

## Extension Methods

Extension methods are supported.  However, there is a special case of Generic Extension methods applied to interfaces that required a significant amount of reflective inspection.

The reason "generic extension methods applied to interfaces" are difficult is because we end up having to inject those methods applied to the concrete generic arguments in the derived classes.  Here is a specific example - just in C#:

A random interface someone declares:

```
interface IFoo<T>{
}
```

A random generic extension method that someone applies to that interface:

```
class Extensions{
   public void Bar(this IFoo<T> foo, T t){
   }
}
```

A random derived class that inherits from that interface:

```
class Derived : IFoo<string>{
}
```


The above Derived now needs to have the following injected into it (in typescript declaration file - yes in the syntax of typescript):

```
class Derived : IFoo<string>{
  public void Bar(string t)
}
```

Note the parameter is now typed as "string" and not "T".
