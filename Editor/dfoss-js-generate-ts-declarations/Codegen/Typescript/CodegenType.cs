using System;
using System.Reflection;
using System.Collections.Generic;

namespace Javascript.Codegen.Dts
{
	public class CodegenType
	{
		private Type _type;

		public Type Type
		{
			get { return _type; }
			private set { _type = value; }
		}

		public bool HasSubscriptOverload { get; private set; }
		public bool IsIReadOnlyList { get; private set; }

		private ICodeGenLogger _logger;
		private TypeTranslator _typeTranslator;

		private List<FieldInfo> _fieldInfos = new List<FieldInfo>();
		// TPC: Using IReadOnlyList<FieldInfo> in case we decide to return .ToArray() one day
		public IReadOnlyList<FieldInfo> FieldInfos
		{
			get
			{
				return _fieldInfos;
			}
		}
		private List<EventInfo> _eventInfos = new List<EventInfo>();
		// TPC: Using IReadOnlyList<EventInfo> in case we decide to return .ToArray() one day
		public IReadOnlyList<EventInfo> EventInfos
		{
			get
			{
				return _eventInfos;
			}
		}
		private List<PropertyInfo> _propertyInfos = new List<PropertyInfo>();
		// TPC: Using IReadOnlyList<PropertyInfo> in case we decide to return .ToArray() one day
		public IReadOnlyList<PropertyInfo> PropertyInfos
		{
			get
			{
				// TPC: the following gives us determinism (yay) - but it would be better/nicer if we could just maintain the ordering from C#
				/*_propertyInfos.Sort((x, y) =>
                {
                    return x.Name.CompareTo(y.Name);
                });
                */
				return _propertyInfos;
			}
		}
		private List<ConstructorInfo> _constructorInfos = new List<ConstructorInfo>();
		// TPC: Using IReadOnlyList<MethodBase> in case we decide to return .ToArray() one day
		public IReadOnlyList<MethodBase> ConstructorInfos
		{
			get
			{
				return _constructorInfos;
			}
		}
		private List<MethodInfo> _methodInfos = new List<MethodInfo>();
		// TPC: Using IReadOnlyList<MethodBase> in case we decide to return .ToArray() one day
		public IReadOnlyList<MethodBase> MethodInfos
		{
			get
			{
				// TPC: the following gives us determinism (yay) - but it would be better/nicer if we could just maintain the ordering from C#
				/*_methodInfos.Sort((x, y) =>
                {
                    return x.Name.CompareTo(y.Name);
                });
                */
				return _methodInfos;
			}
		}

		private List<MethodInfo> _extensionMethods = new List<MethodInfo>();
		// TPC: Using IReadOnlyList<MethodBase> in case we decide to return .ToArray() one day
		public IReadOnlyList<MethodBase> ExtensionMethods
		{
			get
			{
				return _extensionMethods;
			}
		}

		// TPC: these extensions methods are a bit "special" - in that they're not "directly" attached to this type - but instead to something in the
		// inheritance hierarchy of this type
		private GenericInterfaceArgs? _derivedExtensionMethods;
		public GenericInterfaceArgs? DerivedExtensionMethods
		{
			get
			{
				return _derivedExtensionMethods;
			}
		}

		private readonly Type ReadOnlyListType = typeof(IReadOnlyList<>);

		public CodegenType(ICodeGenLogger logger, Type type, TypeTranslator typeTranslator)
		{
			_logger = logger;
			_type = type;
			_typeTranslator = typeTranslator;
			// we're registering ourselves before we inspect the type in case this class/type uses an instance of itself - exp copy constructor:
			// class Foo { Foo(Foo foo){} }
			_typeTranslator.AddExportedType(this);
			InspectType();
		}

		public string TypescriptNamespace
		{
			get
			{
				if (_type.DeclaringType != null)
				{
					var namespaceName = string.IsNullOrEmpty(_type.Namespace)
						? _type.DeclaringType.Name
						: string.Format("{0}.{1}", _type.Namespace, _type.DeclaringType.Name);
					return namespaceName;
				}
				else
				{
					var namespaceName = _type.Namespace ?? "";
					return namespaceName;
				}
			}
		}

		private static readonly System.Collections.Generic.HashSet<Type> ValTupleTypes = new HashSet<Type>(
			new Type[] { typeof(ValueTuple<>), typeof(ValueTuple<,>),
				 typeof(ValueTuple<,,>), typeof(ValueTuple<,,,>),
				 typeof(ValueTuple<,,,,>), typeof(ValueTuple<,,,,,>),
				 typeof(ValueTuple<,,,,,,>), typeof(ValueTuple<,,,,,,,>)
			}
		);

		private bool IsValueTuple(Type type)
		{
			return ValTupleTypes.Contains(type);
		}

		private Type[]? GetValueTupleTypeArguments(Type type)
		{
			var isGenericType = type.IsGenericType;
			if (!isGenericType)
			{
				return null;
			}

			// TPC: this single statement took me hours to figure out - that is - you have to "pull out the type def" to get at the ValueTuple
			// AND even then - performaing a "type check" against ITuple doesn't work (i.e. obj as ITuple is still false)
			var genericTypeDef = type.GetGenericTypeDefinition();

			// TPC: no idea why neither of the following statements work
			/*
            System.Runtime.CompilerServices.ITuple tuple = type as System.Runtime.CompilerServices.ITuple;
            System.Runtime.CompilerServices.ITuple genericTuple = genericTypeDef as System.Runtime.CompilerServices.ITuple;
            _logger.Debug("GetValueTupleTypeArguments: type: " + type + " genericTypeDef: " + genericTypeDef + " tuple: " + (tuple != null) + " genericTuple: " + (genericTuple != null) + " IsValueTuple: " + IsValueTuple(genericTypeDef) + " IsValueTuple2: " + IsValueTuple(type));
            */
			if (!IsValueTuple(genericTypeDef))
			{
				return null;
			}

			//var genericArgs = genericTypeDef.GetGenericArguments();
			var genericArgs = type.GetGenericArguments();
			//_logger.Debug("GetValueTupleTypeArguments: type: " + type + " genericArgs.Length: " + genericArgs.Length);
			List<Type> valueTupleArgs = new List<Type>();
			foreach (var genericArg in genericArgs)
			{
				//_logger.Debug("GetValueTupleTypeArguments: type: " + type + " genericArg: " + genericArg);
				valueTupleArgs.Add(genericArg);
			}

			return valueTupleArgs.ToArray();
		}

		public string TraverseGenericArguments(Type[] typeArguments, bool resolveGenericTypeArgs = true)
		{
			int i = 0;
			int lastElementIndex = typeArguments.Length - 1;
			string genericTypeArguments = string.Empty;
			while (true)
			{
				var typeObj = typeArguments[i];
				var valueTupleArgs = GetValueTupleTypeArguments(typeObj);
				if (valueTupleArgs != null) // flatten out the ValueTuple - the "[..]" should probably be lower in the callstack... meh
				{
					genericTypeArguments += "[ " + TraverseGenericArguments(valueTupleArgs) + " ]";
				}
				else
				{
					genericTypeArguments += _typeTranslator.GetTSTypeFullName(typeObj, resolveGenericTypeArgs);
				}
				if (i >= lastElementIndex) // no trailing comma
				{
					break;
				}
				genericTypeArguments += ", ";
				++i;
			}
			return genericTypeArguments;
		}

		// TPC: the following method was my attempt to pull out template/generic arguments - but there is a post online that might have a better approach:
		// https://stackoverflow.com/questions/2448800/given-a-type-instance-how-to-get-generic-type-name-in-c
		public string GetGenericArguments(Type? genericTypeArgProvider = null)
		{
			string genericTypeArguments = "";
			if (!_type.IsGenericType)
			{
				return genericTypeArguments;
			}

			// "genericType.GenericTypeArguments" return an empty array if the caller uses something like "typeof(System.IObserver<>)"
			Type resolvedType = _type.GetGenericTypeDefinition();
			var typeArguments = resolvedType.GetGenericArguments();
			if (genericTypeArgProvider != null)
			{
				typeArguments = genericTypeArgProvider.GetGenericArguments();
			}

			if (typeArguments.Length == 0)
			{
				throw new System.Exception("TypescriptDeclarationGenerator: AppendClass: this: " + _type + " is apparently a 'GenericType' (" + resolvedType + ") - but has no type arguments - not sure how to handle that");
			}
			genericTypeArguments = "<";

			bool resolveGenericTypeArgs = true;
			if (genericTypeArgProvider == null)
			{
				resolveGenericTypeArgs = false;
			}
			genericTypeArguments += TraverseGenericArguments(typeArguments, resolveGenericTypeArgs);

			genericTypeArguments += ">";

			return genericTypeArguments;
		}

		public string TypescriptTypeName(bool includeNamespace, Type? genericTypeArgProvider = null)
		{
			var fullyScopedName = _type.Name;
			if (_type.IsGenericType)
			{
				// TPC: allegedly this bizarre syntax (the backtick in the type name) has to do with how the CLR could be generated from multiple source languages...
				var backtickIndex = fullyScopedName.IndexOf('`');
				if (backtickIndex < 0)
				{
					_logger.Warn("CodegenType: TypescriptTypeName: _type: " + _type + " is allegedly a generic type - but its name does not include a back tick.  Type.Name: " + fullyScopedName);
				}
				else
				{
					fullyScopedName = fullyScopedName.Substring(0, backtickIndex);
				}
				//fullyScopedName += GetGenericArguments(resolveGenericArgs);
				fullyScopedName += GetGenericArguments(genericTypeArgProvider);
			}
			// TPC: .NET is a bit crap - Type.FullName doesn't do what you would expect for nested classes
			// exp: namespace Foo { class Bar { } }
			// it returns Foo.Bar as you would expect - however:
			// class Foo { class Bar { } }
			// Foo+Bar
			var namespaceName = TypescriptNamespace;
			if ((includeNamespace) && (!string.IsNullOrEmpty(namespaceName)))
			{
				fullyScopedName = namespaceName + "." + fullyScopedName;
			}
			return fullyScopedName;
		}

		public string FullTypeName
		{
			get
			{
				var typeName = (_type.FullName != null) ? _type.FullName : _type.Name;
				return typeName;
			}
		}

		public string GetFileName()
		{
			if (_type.IsGenericType)
			{
				var selfname = string.IsNullOrEmpty(_type.Namespace) ? "" : (_type.Namespace + "_");
				var backtickIndex = _type.Name.IndexOf('`');
				if (backtickIndex >= 0)
				{
					selfname += _type.Name.Substring(0, backtickIndex);
				}
				foreach (var gp in _type.GetGenericArguments())
				{
					selfname += "_" + gp.Name;
				}
				return selfname.Replace(".", "_").Replace("<", "_").Replace(">", "_").Replace("`", "_");
			}
			var typeName = FullTypeName;
			var filename = typeName.Replace(".", "_").Replace("<", "_").Replace(">", "_").Replace("`", "_");
			return "Gen_" + filename;
		}

		public string GetImplementInterfacesString()
		{
			var interfaces = _type.GetInterfaces();
			List<string> interfacesList = new List<string>();
			foreach (var interfaceType in interfaces)
			{
				var codegenTypeInterface = _typeTranslator.GetExportedType(interfaceType);
				if (codegenTypeInterface == null)
				{
					_logger.Warn("GetImplementInterfacesString: type: " + _type + " implements interface: " + interfaceType + " but we're skipping it because it isn't a registered type");
					continue;
				}
				var fullyScopedInterfaceName = _typeTranslator.GetTSTypeFullName(interfaceType);
				//_logger.Debug("GetImplementInterfacesString: fullyScoped: " + fullyScopedInterfaceName);
				interfacesList.Add(fullyScopedInterfaceName);
			}

			return string.Join(", ", interfacesList);
		}

		private void AddField(FieldInfo fieldInfo)
		{
			_fieldInfos.Add(fieldInfo);
		}

		private void AddEvent(EventInfo eventInfo)
		{
			_eventInfos.Add(eventInfo);
		}

		private void AddProperty(PropertyInfo propertyInfo)
		{
			_propertyInfos.Add(propertyInfo);
		}

		private void AddConstructor(ConstructorInfo constructorInfo)
		{
			_constructorInfos.Add(constructorInfo);
		}

		private void AddMethod(MethodInfo methodInfo)
		{
			_methodInfos.Add(methodInfo);
		}

		public void AddExtensionMethod(MethodInfo methodInfo)
		{
			//_logger.Debug("CodegenType: AddExtensionMethod: type: " + _type + " methodInfo: " + methodInfo);
			_extensionMethods.Add(methodInfo);
		}

		public void AddDerivedExtensionMethods (GenericInterfaceArgs genericInterfaceArgs)
		{
			_derivedExtensionMethods = genericInterfaceArgs;
		}

		private bool CheckIsIReadOnlyList()
		{
			if ((_type.IsGenericType) && (_type.GetGenericTypeDefinition() == ReadOnlyListType))
			{
				//Console.WriteLine("this is IReadOnlyList: " + _type + " interfaceType: " + interfaceType);
				return true;
			}

			var interfaces = _type.GetInterfaces();
			for (int i = 0; i < interfaces.Length; ++i)
			{
				var interfaceType = interfaces[i];
				if ((interfaceType.IsGenericType) && (interfaceType.GetGenericTypeDefinition() == ReadOnlyListType))
				{
					//Console.WriteLine("generic interface parenting: " + _type + " interfaceType: " + interfaceType);
					return true;
				}
			}
			return false;
		}

		private void InspectType()
		{
			var bindingFlags = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static;
			// TPC: TODO: we may end up having to do the following (both public and non public fields/methods) because of things like this:
			// class Foo : IObserver<string>{
			//   void IObserver<string>.OnCompleted()
			//var bindingFlags = BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static;
			var fields = _type.GetFields(bindingFlags);
			foreach (var field in fields)
			{
				if (field.IsSpecialName)
				{
					_logger.Debug("On type: {0} skipping special field: {1}", _type, field.Name);
					continue;
				}
				if (field.FieldType.IsPointer)
				{
					_logger.Debug("On type: {0} skipping pointer field: {1}", _type, field.Name);
					continue;
				}
				if (field.IsDefined(typeof(SkipGenerateAttribute), false))
				{
					_logger.Debug("On type: {0} skipping omitted field: {1}", _type, field.Name);
					continue;
				}
				if (field.IsDefined(typeof(ObsoleteAttribute), false))
				{
					_logger.Debug("On type: {0} skipping obsolete field: {1}", _type, field.Name);
					continue;
				}
				/*if (transform != null && transform.IsMemberBlocked(field.Name))
                {
                    _logger.Debug("skip blocked field: {0}", field.Name);
                    continue;
                }
                */
				AddField(field);
			}
			var events = _type.GetEvents(bindingFlags);
			foreach (var evt in events)
			{
				if (evt.IsSpecialName)
				{
					_logger.Debug("On type: {0} skipping special event: {1}", _type, evt.Name);
					continue;
				}
				var evtHandlerType = evt.EventHandlerType;
				if ((evtHandlerType != null) && (evtHandlerType.IsPointer))
				{
					_logger.Debug("On type: {0} skipping pointer event: {1}", _type, evt.Name);
					continue;
				}
				if (evt.IsDefined(typeof(ObsoleteAttribute), false))
				{
					_logger.Debug("On type: {0} skipping obsolete event: {1}", _type, evt.Name);
					continue;
				}
				if (evt.IsDefined(typeof(SkipGenerateAttribute), false))
				{
					_logger.Debug("On type: {0} skipping omitted event: {1}", _type, evt.Name);
					continue;
				}
				/*
                if (transform != null && transform.IsMemberBlocked(evt.Name))
                {
                    _logger.Debug("skip blocked event: {0}", evt.Name);
                    continue;
                }
                */
				AddEvent(evt);
			}
			var properties = _type.GetProperties(bindingFlags);
			foreach (var property in properties)
			{
				//_logger.Debug("InspectType: property: " + property.Name);
				if (property.IsSpecialName)
				{
					_logger.Debug("On type: {0} skipping special property: {1}", _type, property.Name);
					continue;
				}
				if (property.PropertyType.IsPointer)
				{
					_logger.Debug("On type: {0} skipping pointer property: {1}", _type, property.Name);
					continue;
				}
				if (property.IsDefined(typeof(SkipGenerateAttribute), false))
				{
					_logger.Debug("On type: {0} skipping omitted property: {1}", _type, property.Name);
					continue;
				}
				if (property.IsDefined(typeof(ObsoleteAttribute), false))
				{
					_logger.Debug("On type: {0} skipping obsolete property: {1}", _type, property.Name);
					continue;
				}
				/*if (transform != null && transform.IsMemberBlocked(property.Name))
                {
                    _logger.Debug("skip blocked property: {0}", property.Name);
                    continue;
                }
                */
				if (property.Name == "Item")
				{
					// TPC: not sure if we should make these two mutually exclusive - i.e. if IsIReadOnlyList is true - then HasSubscriptOverload should be false?
					if (CheckIsIReadOnlyList())
					{
						IsIReadOnlyList = true;
					}
					
					HasSubscriptOverload = true;
					//_logger.Debug("InspectType: type: " + _type.FullName + " overloads the subscript operator");
					continue;
				}
				//_logger.Debug("InspectType: adding property: " + property.Name);
				AddProperty(property);
			}
			if (!_type.IsAbstract)
			{
				var constructors = _type.GetConstructors();
				foreach (var constructor in constructors)
				{
					//if (constructor.IsDefined(typeof(JSOmitAttribute), false))
					if (constructor.IsDefined(typeof(SkipGenerateAttribute), false))
					{
						_logger.Debug("On type: {0} skipping omitted constructor: {1}", _type, constructor);
						continue;
					}
					if (constructor.IsDefined(typeof(ObsoleteAttribute), false))
					{
						_logger.Debug("On type: {0} skipping obsolete constructor: {1}", constructor);
						continue;
					}
					/*if (BindingManager.ContainsPointer(constructor))
                    {
                        _logger.Debug("skip pointer constructor: {0}", constructor);
                        continue;
                    }
                    */
					AddConstructor(constructor);
				}
			}
			var methods = _type.GetMethods(bindingFlags);
			foreach (var method in methods)
			{
				/*if (method == null)
				{
                    continue;
				}
                */

				/*
                if (BindingManager.IsGenericMethod(method))
                {
                    _logger.Debug("skip generic method: {0}", method);
                    continue;
                }
                if (BindingManager.ContainsPointer(method))
                {
                    _logger.Debug("skip unsafe (pointer) method: {0}", method);
                    continue;
                }
                */
				if (method.IsSpecialName)
				{
					//_logger.Debug("InspectType: (method != null): " + (method != null));
					_logger.Debug("On type: {0} skipping special method: {1}", _type, method);
					continue;
				}
				//if (method.IsDefined(typeof(JSOmitAttribute), false))
				if (method.IsDefined(typeof(SkipGenerateAttribute), false))
				{
					_logger.Debug("On type: {0} skipping omitted method: {1}", _type, method);
					continue;
				}
				if (method.IsDefined(typeof(ObsoleteAttribute), false))
				{
					_logger.Debug("On type: {0} skipping obsolete method: {1}", _type, method);
					continue;
				}
				/*
                if (transform != null && transform.IsMemberBlocked(method.Name))
                {
                    _logger.Debug("skip blocked method: {0}", method.Name);
                    continue;
                }
                */

				// if (IsPropertyMethod(method))
				// {
				//     continue;
				// }
				/*
                do
                {
                    if (IsExtensionMethod(method))
                    {
                        var targetType = method.GetParameters()[0].ParameterType;
                        var targetInfo = bindingManager.GetExportedType(targetType);
                        if (targetInfo != null)
                        {
                            targetInfo.AddMethod(method);
                            break;
                        }
                        // else fallthrough (as normal static method)
                    }
                    AddMethod(method);
                } while (false);
                */

				AddMethod(method);
			}
		}
	}

	/// <summary>
	/// Example of mapping from C# to types:
	/// C#
	/// class Foo : Bar<string>
	///
	/// in the above example:
	/// GenericType: Bar<string>
	/// ConcreteType: Foo
	/// 
	/// </summary>
	public class GenericToConcreteType
	{
		public Type GenericType;
		public Type ConcreteType;

		public List<MethodInfo> Methods = new List<MethodInfo>();

		public GenericToConcreteType(Type genericType, Type concreteType, IReadOnlyList<MethodInfo> methods)
		{
			GenericType = genericType;
			ConcreteType = concreteType;
			Methods.AddRange(methods);
		}
	}

	public class GenericInterfaceArgs
	{
		public List<GenericToConcreteType> GenericToConcreteTypes = new List<GenericToConcreteType>();

		public GenericInterfaceArgs(IReadOnlyList<GenericToConcreteType> genericToConcreteTypes)
		{
			GenericToConcreteTypes.AddRange(genericToConcreteTypes);
		}
	}
}

