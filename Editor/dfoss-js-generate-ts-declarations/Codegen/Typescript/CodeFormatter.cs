using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Javascript.Codegen.Dts
{
    public class CodeFormatter
    {
        public bool enabled = true;
        private string newline;
        private string tab;
        private StringBuilder sb = new StringBuilder();
        private int tabLevel;
        private int _bracketLevel = 0;

        public int size { get { return sb.Length; } }

        public CodeFormatter(string tab = "\t", string? newline = null)
        {
            if (newline == null)
			{
                newline = System.Environment.NewLine;
            }
            this.newline = newline;
            this.tab = tab;
            this.tabLevel = 0;
        }

        public override string ToString()
        {
            EndScopes();
            return sb.ToString();
        }

        private void AddTabLevel()
        {
            tabLevel++;
        }

        private void DecTabLevel()
        {
            tabLevel--;
        }

        private void AppendTab()
        {
            for (var i = 0; i < tabLevel; i++)
            {
                sb.Append(tab);
            }
        }

        public void Append(string format, params string[] args)
        {
            AppendTab();
            sb.AppendFormat(format, args);
        }

        public void AppendLine(string format, params string[] args)
        {
            Append(format, args);
            sb.Append(newline);
        }

        public void AppendBracket()
		{
            sb.Append("{");
            sb.Append(newline);
            ++_bracketLevel;
            AddTabLevel();
        }
        
        private void EndScopes()
		{
            for (int i = 0; i < _bracketLevel; ++i)
			{
                DecTabLevel();
                AppendTab();
                sb.Append("}");
                sb.Append(newline);
            }
            _bracketLevel = 0;
        }

        public void Clear()
        {
            tabLevel = 0;
            sb.Clear();
        }

        public void GetCodeCommenting(DocResolver.DocBody docBody)
		{
            if (docBody == null)
			{
                return;
			}
            var docSummary = docBody.summary;
            if (docSummary == null)
			{
                throw new System.Exception("GetCodeCommenting: DocBody.Summary (for: " + docBody + ") is null!");
			}
            if (docSummary.Length > 1)
            {
                AppendLine("/**");
                foreach (var line in docSummary)
                {
                    AppendLine(" * {0}{1}", line.Replace('\r', ' '));
                }
            }
            else
            {
                if (docSummary.Length == 0 || string.IsNullOrEmpty(docSummary[0]))
                {
                    if (docBody.parameters.Count == 0 && string.IsNullOrEmpty(docBody.returns))
                    {
                        return;
                    }
                    AppendLine("/**");
                }
                else
                {
                    AppendLine("/** {0}", docSummary[0]);
                }
            }
            foreach (var kv in docBody.parameters)
            {
                var pname = kv.Key;
                var ptext = kv.Value;
                AppendLine($" * @param {pname} {ptext}");
            }
            if (!string.IsNullOrEmpty(docBody.returns))
            {
                AppendLine($" * @returns {docBody.returns}");
            }
            AppendLine(" */");
        }
    }
}
