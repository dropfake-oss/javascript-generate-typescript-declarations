﻿using System;

namespace Javascript.Codegen.Dts
{
	public class ConsoleCodeGenLogger : ICodeGenLogger
	{
		public void Info(string format, params object[] args)
		{
			Console.WriteLine(format, args);
		}

		public void Debug(string format, params object[] args)
		{
			Console.WriteLine(format, args);
		}

		public void Warn(string format, params object[] args)
		{
			var originalForeColor = Console.ForegroundColor;
			try
			{
				Console.ForegroundColor = ConsoleColor.Yellow;
				Console.WriteLine(format, args);
			}
			finally
			{
				Console.ForegroundColor = originalForeColor;
			}
		}

		public void Error(string format, params object[] args)
		{
			var originalForeColor = Console.ForegroundColor;
			try
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(format, args);
			}
			finally
			{
				Console.ForegroundColor = originalForeColor;
			}
		}
	}
}
