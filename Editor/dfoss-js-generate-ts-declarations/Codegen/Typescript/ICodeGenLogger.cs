﻿

namespace Javascript.Codegen.Dts
{
	public interface ICodeGenLogger
	{
		// TPC: TODO: once this gets into its own package (with nullable enabled) - make the following interfaces:
		void Debug(string format, params object[] args);
		void Info(string format, params object[] args);
		void Warn(string format, params object[] args);
		void Error(string format, params object[] args);

		// into this:
		//void Debug(string format, params object?[] args);
		//void Info(string format, params object?[] args);
		//void Warn(string format, params object?[] args);
		//void Error(string format, params object?[] args);
	}
}
