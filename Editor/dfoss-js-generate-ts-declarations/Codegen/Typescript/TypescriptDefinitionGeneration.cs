using System;
using System.Reflection;
using System.Collections.Generic;

namespace Javascript.Codegen.Dts
{
	public delegate void GeneratedTypeDelegate(TypescriptDeclarationGenerator typescriptDeclarationGenerator);

	public class CodeGenAssembly
	{
		public Assembly Assembly;
		public bool CodeGenEntireAssembly;

		public CodeGenAssembly(Assembly assembly, bool codegenEntireAssembly)
		{
			Assembly = assembly;
			CodeGenEntireAssembly = codegenEntireAssembly;
		}
	}

	public class TypescriptDefinitionGeneration
	{
		private ICodeGenLogger _logger;
		private GeneratedTypeDelegate _generatedTypeCallback;
		private TypeTranslator _typeTranslator;

		private Dictionary<Type, TypescriptDeclarationGenerator> _typescriptDeclarationGenerators = new Dictionary<Type, TypescriptDeclarationGenerator>();

		private HashSet<Assembly> _codegenEntireAssembly = new HashSet<Assembly>();

		private HashSet<Type> _registeredTypes = new HashSet<Type>();

		public TypescriptDefinitionGeneration(ICodeGenLogger logger, TypeTranslator typeTranslator, GeneratedTypeDelegate generatedTypeCallback)
		{
			_logger = logger;
			// TPC: TODO: replace static accessors from DocResolver
			DocResolver.Logger = _logger;
			_typeTranslator = typeTranslator;

			HashSet<CodegenType> emittedTypes = new HashSet<CodegenType>();
			_generatedTypeCallback = (TypescriptDeclarationGenerator typescriptDeclarationGenerator) =>
			{
				generatedTypeCallback(typescriptDeclarationGenerator);
				emittedTypes.Add(typescriptDeclarationGenerator.CodeGenType);
			};

			ExportBuiltins();
		}

		private void InjectExtensionMethods(CodeGenAssembly codeGenAssembly)
		{
			Assembly assembly = codeGenAssembly.Assembly;
			var types = assembly.GetTypes();
			//_logger.Debug("TypescriptDefinitionGeneration: InjectExtensionMethods: types: " + types.Length + " assembly: " + assembly.FullName);
			foreach (var type in types)
			{
				var hasExtensionAttribute = type.IsDefined(typeof(System.Runtime.CompilerServices.ExtensionAttribute), false);
				//if (type.Name.Contains("ObservableExtensions"))
				//{
				//	_logger.Debug("TypescriptDefinitionGeneration: InjectExtensionMethods: type: " + type + " hasExtensionAttribute: " + hasExtensionAttribute + " IsSealed: " + type.IsSealed + " IsGenericType: " + type.IsGenericType + " IsNested : " + type.IsNested + " assembly: " + assembly.FullName);
				//}
				// TPC: now that we're trying to support Generic types - we probably don't need to skip Generic types anymore - but I'd prefer to hold off until we have a test for this
				if (type.IsGenericType)
				{
					continue;
				}
				// TPC: according to online doc's the following "Methods filter" should've got us Extension method - but it appears to filter them out
				//var methodInfos = type.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				//var methodInfos = type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic);
				var methodInfos = type.GetMethods();
				//_logger.Debug("TypescriptDefinitionGeneration: InjectExtensionMethods: type: " + type.FullName + " methodInfos: " + methodInfos.Length);
				foreach (var methodInfo in methodInfos)
				{
					// TPC: now that we're trying to support Generic types - don't need to skip those extension methods anymore
					var isExtensionAttribute = methodInfo.IsDefined(typeof(System.Runtime.CompilerServices.ExtensionAttribute), false);
					//if (type.Name.Contains("ObservableExtensions"))
					//{
					//	_logger.Debug("TypescriptDefinitionGeneration: InjectExtensionMethods: type: " + type.FullName + " method: " + methodInfo + " isExtensionAttribute: " + isExtensionAttribute);
					//}
					if (!isExtensionAttribute)
					{
						continue;
					}

					var parameters = methodInfo.GetParameters();
					var injectedType = parameters[0].ParameterType;
					bool canExport = ExportedType(injectedType);
					//if (type.Name.Contains("ObservableExtensions"))
					//{
					//	_logger.Debug("TypescriptDefinitionGeneration: InjectExtensionMethods: type: " + type.FullName + " method: " + methodInfo.Name + " canExport: " + canExport + " injectedType: " + injectedType);
					//}
					if (injectedType.IsArray) // TPC: not sure how to support extension methods on arrays
					{
						continue;
					}

					if (canExport)
					{
						TypescriptDeclarationGenerator? typescriptDeclarationGenerator = GetRegisteredTypescriptGenerator(injectedType);
						if (typescriptDeclarationGenerator == null)
						{
							throw new System.Exception("InjectExtensionMethods: type: " + type + " is trying to use injectedType: " + injectedType + " which is not found in _typescriptDeclarationGenerators for method: " + methodInfo.Name);
						}
						//_logger.Debug("TypescriptDefinitionGeneration: InjectExtensionMethods: type: " + type.FullName + " adding extension method: " + methodInfo + " to CodeGenType");
						typescriptDeclarationGenerator.CodeGenType.AddExtensionMethod(methodInfo);

						RegisterExtensionMethodOnType(typescriptDeclarationGenerator.CodeGenType.Type, methodInfo);
					}
				}
			}
		}

		private Dictionary<Type, List<MethodInfo>> _extensionMethods = new Dictionary<Type, List<MethodInfo>>();

		private void RegisterExtensionMethodOnType(Type type, MethodInfo methodInfo)
		{
			List<MethodInfo>? methods;
			if (!_extensionMethods.TryGetValue(type, out methods))
			{
				methods = new List<MethodInfo>();
				_extensionMethods[type] = methods;
			}
			methods.Add(methodInfo);
		}

		private bool ExportedType(Type type)
		{
			Type actualType = type;

			// TPC: Not sure the best place to filter out nested classes (because typescript doesn't properly support nested classes) - though there is a work-around:
			// https://stackoverflow.com/questions/32494174/can-you-create-nested-classes-in-typescript
			// so if we decide this is important - we can add support
			if (actualType.IsNested)
			{
				// TPC: let's try to add limited support for nested types - specifically start with enums
				// we're using the the technique mentioned here:
				// https://stackoverflow.com/questions/29844959/enum-inside-class-typescript-definition-file
				// specifically declaration merging:
				// https://www.typescriptlang.org/docs/handbook/declaration-merging.html#merging-namespaces-with-classes
				if (!actualType.IsEnum)
				{
					return false;
				}
			}

			if (type.IsGenericType)
			{
				actualType = type.GetGenericTypeDefinition();
			}
			// TPC: not actually sure how we should deal with arrays - it _feels_ like we should support them - just get the element type and check if that is registered
			if (type.IsArray)
			{
				var elementType = type.GetElementType();
				if (elementType == null)
				{
					throw new System.Exception("ExportedType: type: " + type + " isArray - but GetElementType() returns null!");
				}
				actualType = elementType;
			}

			//ExportedType: type: T[] actualType: T[] _registeredTypes.Contains(actualType): False _codegenEntireAssembly.Contains(type.Assembly): True type.IsDefined(typeof(GenerateTypeAttribute), false): False
			//_logger.Debug("ExportedType: type: " + type + " actualType: " + actualType + " _registeredTypes.Contains(actualType): " + _registeredTypes.Contains(actualType) + " _codegenEntireAssembly.Contains(type.Assembly): " + _codegenEntireAssembly.Contains(type.Assembly) + " type.IsDefined(typeof(GenerateTypeAttribute), false): " + type.IsDefined(typeof(GenerateTypeAttribute), false));

			if (_registeredTypes.Contains(actualType))
			{
				return true;
			}

			// TPC: not sure if we need to filter Extension classes - so we don't end up with duplicates (i.e. the class containing its extension methods and the extension class that contributes those extension methods)
			/*var hasExtensionAttribute = type.IsDefined(typeof(System.Runtime.CompilerServices.ExtensionAttribute), false);
			if (hasExtensionAttribute)
			{
				//return false;
				_logger.Debug("TypescriptDefinitionGeneration: ExportedType: type: " + type + " hasExtensionAttribute: " + hasExtensionAttribute);
			}
			*/

			if (_codegenEntireAssembly.Contains(type.Assembly))
			{
				// TPC: now that we're trying to support Generic types - don't need to skip generics anymore (i.e. don't need to return false for type.IsGenericTypeDefinition)
				// BUT - for now - we don't support binding extension methods to "all array" types (for example) - i.e. this
				//public static ArrayIterator<T> Begin<T>(this T[] array)
				//_logger.Debug("ExportedType: type: " + type + " actualType: " + actualType + " IsConstructedGenericType: " + type.IsConstructedGenericType + " type.IsGenericTypeDefinition: " + type.IsGenericTypeDefinition + " actualType.IsGenericTypeDefinition: " + actualType.IsGenericTypeDefinition + " IsGenericParameter: " + type.IsGenericParameter + " IsGenericMethodParameter: " + type.IsGenericMethodParameter + " IsGenericType: " + type.IsGenericType + " IsGenericTypeParameter: " + type.IsGenericTypeParameter);

				return true;
			}
			if (type.IsDefined(typeof(GenerateTypeAttribute), false))
			{
				return true;
			}
			return false;
		}

		public void RegisterType(Type type)
		{
			var actualType = type;
			if (type.IsGenericType)
			{
				actualType = type.GetGenericTypeDefinition();
			}
			_registeredTypes.Add(actualType);
			// TPC: do we need to recurse on this method if the type is not GenericType
			//_logger.Debug("CreateTypescriptDeclarationGenerator Type: {0} GenericType: {1} ", type.FullName, type.IsGenericType);
			TypescriptDeclarationGenerator typescriptDeclarationGenerator = new TypescriptDeclarationGenerator(_logger, _typeTranslator, type);
			_typescriptDeclarationGenerators[type] = typescriptDeclarationGenerator;
		}

		private TypescriptDeclarationGenerator? GetRegisteredTypescriptGenerator(Type type)
		{
			var actualType = type;
			if (type.IsGenericType)
			{
				actualType = type.GetGenericTypeDefinition();
			}
			TypescriptDeclarationGenerator? typescriptDeclarationGenerator;
			_typescriptDeclarationGenerators.TryGetValue(actualType, out typescriptDeclarationGenerator);
			return typescriptDeclarationGenerator;
		}

		private void ExportBuiltins()
		{
			RegisterType(typeof(byte));
			RegisterType(typeof(sbyte));
			RegisterType(typeof(float));
			RegisterType(typeof(double));
			RegisterType(typeof(string));
			RegisterType(typeof(int));
			RegisterType(typeof(uint));
			RegisterType(typeof(short));
			RegisterType(typeof(ushort));
			RegisterType(typeof(object));
			RegisterType(typeof(Array));
			RegisterType(typeof(Object));
		}

		private void CreateTypescriptDeclarationGenerator(Type type)
		{
			if (!ExportedType(type))
			{
				return;
			}

			RegisterType(type);
		}

		private Dictionary<Type, int> _typeDups = new Dictionary<Type, int>();

		private void RegisterGeneratedType(Type type)
		{
			int counter;
			if (_typeDups.TryGetValue(type, out counter))
			{
				_typeDups[type] = (++counter);
			}
			else
			{
				_typeDups[type] = 1;
			}
		}

		private void InjectExtensionMethodsInDerivedClasses(CodegenType codegenType)
		{
			Type thisType = codegenType.Type;
			List<Type> thisTypes = new List<Type>();
			thisTypes.Add(thisType);
			var interfaces = thisType.GetInterfaces();
			// The following lookup is unfortunately necessary because of how we're forced to determine if one type derives from another
			Dictionary<Type, Type> genericToConcreteTypeMap = new Dictionary<Type, Type>();
			for(int i = 0; i < interfaces.Length;++i)
			{
				var interfaceObj = interfaces[i];
				if (interfaceObj.IsGenericType)
				{
					interfaceObj = interfaceObj.GetGenericTypeDefinition();

					genericToConcreteTypeMap[interfaceObj] = interfaces[i];
				}
				thisTypes.Add(interfaceObj);
			}

			foreach(var typeObj in thisTypes)
			{
				//_logger.Debug("InjectExtensionMethodsInDerivedClasses: type: " + codegenType.Type + " typeObj: " + typeObj);
				// TPC: we only need to worry about types that are generic whose extension methods are being injected from an interface
				if (!typeObj.IsGenericType)
				{
					continue;
				}

				List<GenericToConcreteType> genericToConcreteTypes = new List<GenericToConcreteType>();
				foreach (var extensionTypeEntry in _extensionMethods)
				{
					var extensionType = extensionTypeEntry.Key;
					if (!extensionType.IsGenericType) // Object and many other mundane (non generic) classes can have extension methods stuffed into them
					{
						continue;
					}

					// TPC: filter on generic interfaces - also ideally they're also extension methods
					if (!extensionType.IsAssignableFrom(typeObj)) // This is going to be true 99% of the time - we're iterating over all types to see if any type implements a type that has extensions attached
					{
						//_logger.Debug("InjectExtensionMethodsInDerivedClasses: extensionType: " + extensionType + " is not assignable from: " + codegenType.Type);
						continue;
					}
					var extensionMethods = extensionTypeEntry.Value;
					//_logger.Debug("InjectExtensionMethodsInDerivedClasses: type: " + codegenType.Type + " derived from: " + extensionType + " going to add derived extension method: " + extensionMethods.Count);
					var concreteType = typeObj;
					Type? resolvedConcreteType;
					if (genericToConcreteTypeMap.TryGetValue(typeObj, out resolvedConcreteType))
					{
						concreteType = resolvedConcreteType;
					}
					GenericToConcreteType genericToConcreteType = new GenericToConcreteType(extensionType, concreteType, extensionMethods);
					genericToConcreteTypes.Add(genericToConcreteType);
				}

				GenericInterfaceArgs genericInterfaceArgs = new GenericInterfaceArgs(genericToConcreteTypes);
				codegenType.AddDerivedExtensionMethods(genericInterfaceArgs);
			}
		}

		private void GenerateTypescriptDeclaration(Type type)
		{
			if (!ExportedType(type))
			{
				return;
			}

			//_logger.Debug("GenerateTypescriptDeclaration Type: {0} GenericType: {1} ", type.FullName, type.IsGenericType);
			TypescriptDeclarationGenerator? typescriptDeclarationGenerator = GetRegisteredTypescriptGenerator(type);
			if (typescriptDeclarationGenerator == null)
			{
				throw new System.Exception("GenerateTypescriptDeclaration: failed to find TypescriptDeclarationGenerator for type: " + type);
			}

			RegisterGeneratedType(type); // This statement is more for data metrics - i.e. how many types are getting mapped to the same Generator instance
			// TPC: because the same type can resolve to the TypescriptDeclarationGenerator -
			// exp: List<string> and List<int>
			// we have to check if we've already generated for this type
			if (typescriptDeclarationGenerator.GeneratedDefinitions)
			{
				return;
			}
			typescriptDeclarationGenerator.GenerateTypescriptDefinition();
		}

		private void InspectTypes(Type[] types)
		{
			foreach (var type in types)
			{
				CreateTypescriptDeclarationGenerator(type);
			}
		}

		// This method could also be called "InspectAssembly"
		private void BuildTypesIndex(Assembly assembly, bool codegenEntireAssembly)
		{
			if (codegenEntireAssembly)
			{
				_codegenEntireAssembly.Add(assembly);
			}
			var types = assembly.GetExportedTypes();
			InspectTypes(types);
		}

		public void GenerateTypescriptDeclarations(IReadOnlyList<CodeGenAssembly> codegenAssemblies)
		{
			// TPC: we're doing a "two pass" here - in case we need to do any special handling if one custom class references another - exp:
			// class Bar {} class Foo { private Bar bar;}
			// Right now we just naively return Type.FullName when one custom type references another - but we may want to do some aliasing or something
			// of custom/user types
			foreach (var codegenAssembly in codegenAssemblies)
			{
				BuildTypesIndex(codegenAssembly.Assembly, codegenAssembly.CodeGenEntireAssembly);
			}
			foreach (var codegenAssembly in codegenAssemblies)
			{
				InjectExtensionMethods(codegenAssembly);
			}
			foreach (var typescriptDeclGenerator in _typescriptDeclarationGenerators.Values)
			{
				InjectExtensionMethodsInDerivedClasses(typescriptDeclGenerator.CodeGenType);
			}
			foreach (var typescriptDeclGenerator in _typescriptDeclarationGenerators.Values)
			{
				GenerateTypescriptDeclaration(typescriptDeclGenerator.CodeGenType.Type);
			}

			foreach (var typescriptDeclGenerator in _typescriptDeclarationGenerators.Values)
			{
				if (_generatedTypeCallback != null)
				{
					_generatedTypeCallback(typescriptDeclGenerator);
				}
			}

			_logger.Info("Generated: " + _typescriptDeclarationGenerators.Count + " types");
		}
	}
}
