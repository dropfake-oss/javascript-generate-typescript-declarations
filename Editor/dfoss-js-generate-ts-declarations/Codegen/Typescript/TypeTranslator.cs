using System;
using System.Collections.Generic;
using System.Reflection;

namespace Javascript.Codegen.Dts
{
    public class TypeTranslator
    {
        private Dictionary<Type, List<string>> _tsTypeNameMap = new Dictionary<Type, List<string>>();
        private Dictionary<Type, CodegenType> _exportedTypes = new Dictionary<Type, CodegenType>();
        private ICodeGenLogger _logger;

        public TypeTranslator(ICodeGenLogger logger)
        {
            _logger = logger;
            Init();
        }

        private void Init()
        {
            AddTSTypeNameMap(typeof(sbyte), "number");
            AddTSTypeNameMap(typeof(byte), "number");
            AddTSTypeNameMap(typeof(int), "number");
            AddTSTypeNameMap(typeof(uint), "number");
            AddTSTypeNameMap(typeof(short), "number");
            AddTSTypeNameMap(typeof(ushort), "number");
            AddTSTypeNameMap(typeof(long), "number");
            AddTSTypeNameMap(typeof(ulong), "number");
            AddTSTypeNameMap(typeof(float), "number");
            AddTSTypeNameMap(typeof(double), "number");
            AddTSTypeNameMap(typeof(bool), "boolean");
            AddTSTypeNameMap(typeof(string), "string");
            AddTSTypeNameMap(typeof(char), "string");
            AddTSTypeNameMap(typeof(void), "void");
        }

        public CodegenType[] GetExportedTypes()
		{
            List<CodegenType> codegenTypes = new List<CodegenType>();
            foreach (var codegenType in _exportedTypes.Values)
			{
                codegenTypes.Add(codegenType);
			}
            return codegenTypes.ToArray();
        }

        public void AddTSTypeNameMap(Type type, params string[] names)
        {
            List<string>? list = null;
            if (!_tsTypeNameMap.TryGetValue(type, out list))
            {
                _tsTypeNameMap[type] = list = new List<string>();
            }
            list.AddRange(names);
        }

        public string GetTSArglistTypes(ParameterInfo[] parameters, bool withVarName)
        {
            var size = parameters.Length;
            var arglist = "";
            if (size == 0)
            {
                return arglist;
            }
            for (var i = 0; i < size; i++)
            {
                var parameter = parameters[i];
                var typename = GetTSTypeFullName(parameter.ParameterType);
                if (withVarName)
                {
                    arglist += parameter.Name + ": ";
                }
                arglist += typename;
                if (i != size - 1)
                {
                    arglist += ", ";
                }
            }
            return arglist;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="resolveGenericTypeArguments">"resolving generic type arguments" - which will determine if we pass type into info.TypescriptTypeName(..)</param>
        /// <param name="typeDeclaration"></param>
        /// <returns></returns>
        public string GetTSTypeFullName(Type type, bool resolveGenericTypeArguments, bool typeDeclaration = false)
        {
            if (type == null || type == typeof(void))
            {
                return "void";
            }

            if (!typeDeclaration) // we don't want to have our class declaration to come out like "class UnityEngine.Vector3" - which is what would happen if we pulled out the type from the map
			{
                List<string>? names = null;
                //_logger.Debug("GetTSTypeFullName: type: " + type + " _tsTypeNameMap.TryGetValue(type, out names): " + _tsTypeNameMap.TryGetValue(type, out names) + " GetExportedType(type): " + GetExportedType(type));
                if (_tsTypeNameMap.TryGetValue(type, out names))
                {
                    return names.Count > 1 ? $"({String.Join(" | ", names)})" : names[0];
                }
            }
            if (type.IsArray)
            {
                if (type.GetElementType() == typeof(byte))
                {
                    return "Buffer";
                }
                var elementType = type.GetElementType();
                if (elementType == null)
				{
                    throw new System.Exception("GetTSTypeFullName: type: " + type + " is an array - but the type.GetElementType() returned null");
				}
                return GetTSTypeFullName(elementType) + "[]";
            }
            var info = GetExportedType(type);
            if (info != null)
            {
                ////if (type.IsGenericType)
	            //{
                //  var genericArgs = type.GetGenericArguments();
                //  _logger.Debug("GetTSTypeFullName: type: " + type + " type.IsGenericType: " + type.IsGenericType + " going to return: " + info.TypescriptTypeName(type) + " but need generic params: " + genericArgs.Length);
                //}
                Type? genericTypeArgProvider = null;
                if (resolveGenericTypeArguments)
                {
                    genericTypeArgProvider = type;
                }
                bool includeNamespace = !typeDeclaration; // don't include namespace if we're declaring a type - exp: "class UnityEngine.Vector3"
                return info.TypescriptTypeName(includeNamespace, genericTypeArgProvider);
            }
            // TPC: we probabbly need to handle MulticastDelegates
            if (type.BaseType == typeof(MulticastDelegate))
            {
                var invoke = type.GetMethod("Invoke");
                if (invoke == null)
				{
                    throw new System.Exception("GetTSTypeFullName: type: " + type + " is MulticasatDelegate - but type.GetMethod(\"Invoke\") return null!");
				}
                //_logger.Debug("GetTSTypeFullName: invoke: " + invoke + " type: " + type + " returnType: " + invoke.ReturnType);
                var ret = GetTSTypeFullName(invoke.ReturnType);
                var parameters = invoke.GetParameters();
                var v_arglist = GetTSArglistTypes(parameters, true);
                return $"(({v_arglist}) => {ret})";
            }

            if ((type.IsGenericParameter) || (type.IsGenericTypeParameter))
			{
                // this is likely the scenario of "class Foo<T>" where T is the generic/template type - in this case - we don't want to resolve "T" to "any"
                return type.Name;
            }
            return "any";
        }

        public string GetTSTypeFullName(Type type)
        {
            return GetTSTypeFullName(type, true);
        }

        public void AddExportedType(CodegenType codegenType)
        {
            var dotNetType = codegenType.Type;
            if (codegenType.Type.IsGenericType)
			{
                dotNetType = codegenType.Type.GetGenericTypeDefinition();
			}
            _exportedTypes[dotNetType] = codegenType;
        }

        public CodegenType? GetExportedType(Type type)
		{
            if (type.IsGenericType)
			{
                type = type.GetGenericTypeDefinition();
			}
            CodegenType? codegenType;
            if (_exportedTypes.TryGetValue(type, out codegenType))
			{
                return codegenType;
			}

            return null;
        }
    }
}
