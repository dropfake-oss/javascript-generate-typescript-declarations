using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;

namespace Javascript.Codegen.Dts
{
	public class TypescriptDeclarationGenerator
	{
		private ICodeGenLogger _logger;
		private TypeTranslator _typeTranslator;
		private CodeFormatter _typescriptCodeFormatter;
		public CodeFormatter TypescriptCodeFormatter
		{
			get { return _typescriptCodeFormatter; }
		}
		private CodegenType _codegenType;
		public CodegenType CodeGenType
		{
			get { return _codegenType; }
		}

		private bool _generatedDefinitions = false;
		public bool GeneratedDefinitions
		{
			get { return _generatedDefinitions; }
		}

		public TypescriptDeclarationGenerator(ICodeGenLogger logger, TypeTranslator typeTranslator, Type type)
		{
			ValidateType(type); // TPC: this is just a last line of defense for attempting something we shouldn't (i.e. types we don't yet support)
			_typescriptCodeFormatter = new CodeFormatter();
			_logger = logger;
			_typeTranslator = typeTranslator;

			// TPC: do we need to recursively generate if the type is not GenericType?
			//_logger.Debug("Exporting Type: {0} GenericType: {1} ", type, type.IsGenericType);

			_codegenType = new CodegenType(_logger, type, _typeTranslator);
		}

		private void ValidateType(Type type)
		{
			// TPC: Apparently we can handle all types now... allegedly - add any type checks that don't work in here - we just removed type.IsGenericTypeDefinition - since we're now attempting to support that (generics)
		}

		private void AppendFilePrefix()
		{
			_typescriptCodeFormatter.AppendLine("// Copyright 2022-present Drop Fake Inc. All rights reserved.");
			_typescriptCodeFormatter.AppendLine("// Assembly: {0}", _codegenType.Type.Assembly.GetName().Name);
			_typescriptCodeFormatter.AppendLine("// Type: {0}", _codegenType.FullTypeName);
		}

		private void AppendNamespace()
		{
			var namespaceName = _codegenType.TypescriptNamespace;
			if (!string.IsNullOrEmpty(namespaceName))
			{
				_typescriptCodeFormatter.Append("declare namespace {0} ", namespaceName);
				_typescriptCodeFormatter.AppendBracket();
			}
		}

		// stolen from here:
		// https://stackoverflow.com/questions/58453972/how-to-use-net-reflection-to-check-for-nullable-reference-type
		// bizarre that checking if a parameter is null is more complicated than just: (Nullable.GetUnderlyingType(typeof(T)) != null)
		private static CustomAttributeData? GetAttribute(IEnumerable<CustomAttributeData> customAttributes, string attributeTypeName)
		{
			foreach (var attribute in customAttributes)
			{
				if (attribute.AttributeType.FullName == attributeTypeName)
				{
					return attribute;
				}
			}
			return null;
		}

		private static bool IsNullableHelper(Type memberType, MemberInfo? declaringType, IEnumerable<CustomAttributeData> customAttributes)
		{
			if (memberType.IsValueType)
				return Nullable.GetUnderlyingType(memberType) != null;
			// TPC: we could use Linq instead of using the GetAttribute(..) method below
			//var nullable = customAttributes
			//	.FirstOrDefault(x => x.AttributeType.FullName == "System.Runtime.CompilerServices.NullableAttribute");
			CustomAttributeData? nullable = GetAttribute(customAttributes, "System.Runtime.CompilerServices.NullableAttribute");
			if (nullable != null && nullable.ConstructorArguments.Count == 1)
			{
				var attributeArgument = nullable.ConstructorArguments[0];
				if (attributeArgument.ArgumentType == typeof(byte[]))
				{
					var args = (System.Collections.ObjectModel.ReadOnlyCollection<CustomAttributeTypedArgument>)attributeArgument.Value!;
					if (args.Count > 0 && args[0].ArgumentType == typeof(byte))
					{
						return (byte)args[0].Value! == 2;
					}
				}
				else if (attributeArgument.ArgumentType == typeof(byte))
				{
					return (byte)attributeArgument.Value! == 2;
				}
			}

			for (var type = declaringType; type != null; type = type.DeclaringType)
			{
				// TPC: we could use Linq instead of using the GetAttribute(..) method below
				//var context = type.CustomAttributes
				//	.FirstOrDefault(x => x.AttributeType.FullName == "System.Runtime.CompilerServices.NullableContextAttribute");
				var context = GetAttribute(type.CustomAttributes, "System.Runtime.CompilerServices.NullableContextAttribute");
				if (context != null &&
					context.ConstructorArguments.Count == 1 &&
					context.ConstructorArguments[0].ArgumentType == typeof(byte))
				{
					return (byte)context.ConstructorArguments[0].Value! == 2;
				}
			}

			// Couldn't find a suitable attribute
			return false;
		}

		public static bool IsNullable(ParameterInfo parameter) => IsNullableHelper(parameter.ParameterType, parameter.Member, parameter.CustomAttributes);

		private string EnsureValidVariableName(string variableName)
		{
			if (variableName == "function") // "function" is a keyword in typescript (not in C#) - hence why we have to replace it
			{
				return "func";
			}
			return variableName;
		}

		private string GetMethodParams(MethodBase method, ParameterInfo[] parameters, int startIndex = 0, IReadOnlyList<Type>? paramTypeOverrides = null)
		{
			List<string> parameterStrings = new List<string>();
			for (var i = startIndex; i < parameters.Length; i++)
			{
				var parameter = parameters[i];
				var parameter_prefix = "";
				var parameterType = parameter.ParameterType;
				if (paramTypeOverrides != null)
				{
					parameterType = paramTypeOverrides[i];
				}
				/*if (parameter.IsOut && parameterType.IsByRef)
				{
					refParameters.Add(parameter);
				}
				else if (parameterType.IsByRef)
				{
					
					refParameters.Add(parameter);
				}
				*/
				if (parameter.IsDefined(typeof(ParamArrayAttribute), false) && i == parameters.Length - 1)
				{
					var elementType = parameterType.GetElementType();
					if (elementType == null)
					{
						throw new System.Exception("GetMethodParams: method: " + method.Name + " has parameter: " + parameter + "which has ParamArrayAttribute - but parameterType.GetElementType() returns null");
					}
					var elementTS = _typeTranslator.GetTSTypeFullName(elementType);
					var parameterVarName = EnsureValidVariableName(parameter.Name);
					parameterStrings.Add($"{parameter_prefix}...{parameterVarName}: {elementTS}[]");
				}
				else
				{
					var nullableFlag = "";
					var isNullable = IsNullable(parameter);
					//_logger.Debug("TypescriptDeclarationGenerator: GetMethodparams: method: " + method.Name + " parameter: " + parameter.Name + " parameterType: " + parameterType + " IsOptional: " + parameter.IsOptional + " isNullable: " + isNullable);
					if (isNullable)
					{
						nullableFlag = "?";
					}
					string parameterTS = _typeTranslator.GetTSTypeFullName(parameterType);
					var parameterVarName = EnsureValidVariableName(parameter.Name);
					parameterStrings.Add($"{parameter_prefix}{parameterVarName}{nullableFlag}: {parameterTS}");
				}
			}
			return string.Join(", ", parameterStrings);
		}

		/// <summary>
		/// TPC: the startIndex is solely for extension methods whose first parameter is the type we're injecting into
		/// </summary>
		/// <param name="methodBase"></param>
		/// <param name="startIndex"></param>
		/// <returns></returns>
		private string GetMethodParams(MethodBase methodBase, int startIndex = 0)
		{
			var parameters = methodBase.GetParameters();
			return GetMethodParams(methodBase, parameters, startIndex);
		}

		private void AppendFields(IReadOnlyList<FieldInfo> fieldInfos)
		{
			foreach (var fieldInfo in fieldInfos)
			{
				// TPC: do we need to provide some abstraction/alias for field names?
				var tsFieldVar = fieldInfo.Name;
				var tsFieldPrefix = fieldInfo.IsStatic ? "static " : "";
				if (fieldInfo.IsInitOnly || fieldInfo.IsLiteral) // can't set this field
				{
					tsFieldPrefix += "readonly ";
				}
				var tsFieldType = _typeTranslator.GetTSTypeFullName(fieldInfo.FieldType);

				var doc = DocResolver.GetDocBody(fieldInfo);
				if (doc != null)
				{
					_typescriptCodeFormatter.GetCodeCommenting(doc);
				}

				_typescriptCodeFormatter.AppendLine($"{tsFieldPrefix}{tsFieldVar}: {tsFieldType}");
			}
		}

		private void AppendProperties(IReadOnlyList<PropertyInfo> propertyInfos)
		{
			foreach (var propertyInfo in propertyInfos)
			{
				var tsPropertyVar = propertyInfo.Name;
				var isStatic = false;
				var getProperty = propertyInfo.GetGetMethod();
				if ((getProperty != null) && (getProperty.IsStatic))
				{
					isStatic = true;
				}
				else 
				{
					var setProperty = propertyInfo.GetSetMethod();
					if ((setProperty != null) && (setProperty.IsStatic))
					{
						isStatic = true;
					}
				}
				var tsPropertyPrefix = (isStatic) ? "static " : "";
				if ((!propertyInfo.CanWrite) || (propertyInfo.SetMethod == null) || (!propertyInfo.SetMethod.IsPublic))
				{
					tsPropertyPrefix += "readonly ";
				}
				var tsPropertyType = _typeTranslator.GetTSTypeFullName(propertyInfo.PropertyType);

				var doc = DocResolver.GetDocBody(propertyInfo);
				if (doc != null)
				{
					_typescriptCodeFormatter.GetCodeCommenting(doc);
				}

				_typescriptCodeFormatter.AppendLine($"{tsPropertyPrefix}{tsPropertyVar}: {tsPropertyType}");
			}
		}

		public bool IsExtensionMethod(MethodBase methodBase)
		{
			return methodBase.IsDefined(typeof(System.Runtime.CompilerServices.ExtensionAttribute), false);
		}

		// The code in this method is almost identical to the code in CodegenType.GetGenericArguments(..)
		private string GetMethodGenericArgs(MethodBase method)
		{
			string genericTypeArguments = "";
			if (!method.IsGenericMethod)
			{
				return genericTypeArguments;
			}
			var typeArguments = method.GetGenericArguments();
			if (typeArguments.Length == 0)
			{
				// TPC: I had hoped this situation could be treated as an error - but it happens - exp:
				// TypescriptDeclarationGenerator: GetMethodGenericArgs: this: System.Byte has method: Int32 CompareTo(System.Object) which is apparently a 'GenericType' - but has no type arguments - not sure how to handle that
				_logger.Warn("TypescriptDeclarationGenerator: GetMethodGenericArgs: this: " + this.CodeGenType.Type + " has method: " + method + " which is apparently a 'GenericType' - but has no type arguments - not sure how to handle that");
				return genericTypeArguments;
			}
			genericTypeArguments = "<";

			genericTypeArguments += this.CodeGenType.TraverseGenericArguments(typeArguments, false);

			genericTypeArguments += ">";

			return genericTypeArguments;
		}

		private void AppendMethods(IReadOnlyList<MethodBase> methodInfos)
		{
			if ((methodInfos == null) || (methodInfos.Count == 0))
			{
				return;
			}

			//var firstMethod = methodInfos[0];
			//_logger.Debug("AppendMethods: type: " + firstMethod.DeclaringType + " methodInfos: " + methodInfos.Count);

			foreach(var methodBase in methodInfos)
			{
				// TPC: we've added support for generic methods in JINT - so allowing methodBase.IsGenericMethod methods to be generated now
				var isExtensionMethod = IsExtensionMethod(methodBase);
				//_logger.Debug("AppendMethods: type: " + methodBase.DeclaringType + " method: " + methodBase.Name + " isExtensionMethod: " + isExtensionMethod);
				if ((isExtensionMethod) && (methodBase.IsConstructor))
				{
					throw new System.Exception("TypescriptDefinitionGenerator: AppendMethods: we don't support generic constructors!  method: " + methodBase);
				}

				string prefix = "";
				if (methodBase.IsStatic)
				{
					prefix = "static ";
				}
				string methodName = methodBase.Name;
				string suffix = "";
				if (methodBase.IsConstructor)
				{
					methodName = "constructor";
				}
				else // handle return type
				{
					MethodInfo? methodInfo = methodBase as MethodInfo;
					if (methodInfo == null)
					{
						throw new System.Exception("AppendMethods: how can this be a method - but also not a MethodInfo: " + methodBase);
					}
					var returnTypeTS = _typeTranslator.GetTSTypeFullName(methodInfo.ReturnType);
					suffix = $": {returnTypeTS}";
				}

				var doc = DocResolver.GetDocBody(methodBase);
				if (doc != null)
				{
					_typescriptCodeFormatter.GetCodeCommenting(doc);
				}

				var genericArgs = GetMethodGenericArgs(methodBase);
				string methodPrefix = $"{prefix}{methodName}{genericArgs}(";
				string methodParams = GetMethodParams(methodBase);

				_typescriptCodeFormatter.AppendLine(methodPrefix + methodParams + ")" + suffix);
			}
		}

		private void AppendExtensionMethods(IReadOnlyList<MethodBase> methodInfos)
		{
			//_logger.Debug("AppendExtensionMethods: type: " + firstMethod.DeclaringType + " methodInfos: " + methodInfos.Count);
			//_logger.Debug("AppendExtensionMethods: type: " + _codegenType.Type + " methodInfos: " + methodInfos.Count);
			foreach (var methodBase in methodInfos)
			{
				//var isExtensionMethod = IsExtensionMethod(methodBase);
				//_logger.Debug("AppendExtensionMethods: type: " + methodBase.DeclaringType + " method: " + methodBase.Name + " isExtensionMethod: " + isExtensionMethod);
				//_logger.Debug("AppendExtensionMethods: type: " + _codegenType.Type + " method: " + methodBase.Name + " isExtensionMethod: " + isExtensionMethod);

				MethodInfo? methodInfo = methodBase as MethodInfo;
				if (methodInfo == null)
				{
					throw new System.Exception("AppendExtensionMethods: how can this be an extension method - but not a MethodInfo: " + methodBase);
				}
				string prefix = "";
				string methodName = methodBase.Name;
				var returnTypeTS = _typeTranslator.GetTSTypeFullName(methodInfo.ReturnType);
				string suffix = $": {returnTypeTS}";

				var doc = DocResolver.GetDocBody(methodBase);
				if (doc != null)
				{
					_typescriptCodeFormatter.GetCodeCommenting(doc);
				}

				string methodPrefix = $"{prefix}{methodName}(";
				string methodParams = GetMethodParams(methodBase, 1);

				//_logger.Debug("AppendExtensionMethods: type: " + methodBase.DeclaringType + " where is this ts function: " + (methodPrefix + methodParams + ")" + suffix));
				//_logger.Debug("AppendExtensionMethods: type: " + _codegenType.Type + " where is this ts function: " + (methodPrefix + methodParams + ")" + suffix));
				_typescriptCodeFormatter.AppendLine(methodPrefix + methodParams + ")" + suffix);
			}
		}

		private Type ResolveGenericArgsToConcreteTypes(Type resolveType, Type[] concreteTypes, Type[] genericTypes, string debugText = "")
		{
			if (concreteTypes.Length != genericTypes.Length)
			{
				throw new System.Exception("ResolveGenericArgsToConcreteTypes: " + this._codegenType.Type + " we're currently relying on any derived classes fully providing all types for any base generic types.  Unfortunately in this case - the generic types are: " + genericTypes.Length + " and the concrete types are: " + concreteTypes.Length);
			}

			if (!resolveType.IsGenericType)
			{
				return resolveType;
			}
			if (resolveType.IsGenericType)
			{
				var genericArgs = resolveType.GetGenericArguments();
				//_logger.Debug("ResolveGenericArgsToConcreteTypes: type " + this._codegenType.Type + " debugText: " + debugText + " resolveType: " + resolveType + " genericTypes.Length: " + genericTypes.Length + " genericArgs.Lengt: " + genericArgs.Length);
				if (genericTypes.Length != genericArgs.Length)
				{
					return resolveType;
				}

				for (int i = 0; i < genericArgs.Length;++i)
				{
					var genericArg = genericArgs[i];
					//_logger.Debug("ResolveGenericArgsToConcreteTypes: type " + this._codegenType.Type + " debugText: " + debugText + " comparing: resolveType genericArg: " + genericArg + " with: " + genericTypes[i] + " (genericArg == genericTypes[i]): " + (genericArg == genericTypes[i]));
					// TPC: bizarre - not sure the right way to compare two (unbound) generic arguments - because this seems to fail when two types (exp: T and T) are the same
					//if (genericArg != genericTypes[i])
					if (genericArg.Name != genericTypes[i].Name)
					{
						return resolveType;
					}
				}

				if ((!resolveType.IsGenericTypeDefinition) && (resolveType.IsGenericType))
				{
					resolveType = resolveType.GetGenericTypeDefinition();
				}
				// TPC: this is some crazy voodoo magic:
				var syntheticType = resolveType.MakeGenericType(concreteTypes);
				return syntheticType;
			}
			return resolveType;
		}

		private void AppendDerivedExtensionMethods(GenericInterfaceArgs? genericInterfaceArgs)
		{
			if (genericInterfaceArgs == null)
			{
				return;
			}
			var genericToConcreteTypes = genericInterfaceArgs.GenericToConcreteTypes;
			foreach(var genericToConcreteType in genericToConcreteTypes)
			{
				var concreteType = genericToConcreteType.ConcreteType;

				var derivedConcreteGenericArgs = concreteType.GetGenericArguments();
				var genericBaseType = concreteType.GetGenericTypeDefinition();
				var baseGenericArgs = genericBaseType.GetGenericArguments();

				string genericArgsStr = "";
				foreach (var genericArg in derivedConcreteGenericArgs)
				{
					genericArgsStr += genericArg;
				}
				string genericBaseArgsStr = "";
				foreach (var baseGenericArg in baseGenericArgs)
				{
					genericBaseArgsStr += baseGenericArg;
				}

				var genericType = genericToConcreteType.GenericType;
				var methods = genericToConcreteType.Methods;

				//_logger.Debug("AppendDerivedExtensionMethods: type: " + this._codegenType.Type + " concreteType: " + concreteType + " genericType: " + genericType + " genericArgsStr: " + genericArgsStr + " genericBaseArgsStr: " + genericBaseArgsStr + " methods: " + methods.Count);
				foreach(var method in methods)
				{
					var returnType = method.ReturnType;
					returnType = ResolveGenericArgsToConcreteTypes(returnType, derivedConcreteGenericArgs, baseGenericArgs, method.Name);

					var returnTypeTS = _typeTranslator.GetTSTypeFullName(returnType);
					string suffix = $": {returnTypeTS}";

					var doc = DocResolver.GetDocBody(method);
					if (doc != null)
					{
						_typescriptCodeFormatter.GetCodeCommenting(doc);
					}

					string prefix = "";
					string methodName = method.Name;
					string methodPrefix = $"{prefix}{methodName}(";
					var methodParams = method.GetParameters();
					// Prepopulating this array with the same first element so the elements between the two lists line up
					List<Type> methodParamTypeOverrides = new List<Type>() { methodParams[0].ParameterType };
					for(int i = 1; i < methodParams.Length; ++i)
					{
						var methodParam = methodParams[i];
						var methodParamType = methodParam.ParameterType;
						var resolvedType = ResolveGenericArgsToConcreteTypes(methodParamType, derivedConcreteGenericArgs, baseGenericArgs);
						methodParamTypeOverrides.Add(resolvedType);
					}
					string methodParamsStr = GetMethodParams(method, methodParams, 1, methodParamTypeOverrides);

					//_logger.Debug("AppendExtensionMethods: type: " + methodBase.DeclaringType + " where is this ts function: " + (methodPrefix + methodParams + ")" + suffix));
					//_logger.Debug("AppendExtensionMethods: type: " + _codegenType.Type + " where is this ts function: " + (methodPrefix + methodParams + ")" + suffix));
					_typescriptCodeFormatter.AppendLine(methodPrefix + methodParamsStr + ")" + suffix);
				}
			}
		}

		/// <summary>
		/// TPC: TODO: we need a test case/code so we can validate whether it is working
		/// </summary>
		/// <param name="eventInfos"></param>
		private void AppendEvents(IReadOnlyList<EventInfo> eventInfos)
		{
			if ((eventInfos == null) || (eventInfos.Count == 0))
			{
				return;
			}
			var firstEvent = eventInfos[0];
			_logger.Error("AppendEvents is unimplemented!  Type: {0} has {1} event(s)", (firstEvent.DeclaringType?.ToString() ?? ""), eventInfos.Count); 
			/*foreach (var eventInfo in eventInfos)
			{

			}
			*/
		}

		private string GetBaseClassName(Type type)
		{
			var baseClassName = "";
			var baseClass = type.BaseType;
			while (baseClass != null)
			{
				var exportedType = _typeTranslator.GetExportedType(baseClass);
				if (exportedType != null)
				{
					if (baseClass is System.Object)
					{
						// instead of forcing all classes that have the Item property to derive from Array we might want to consider adding the field () to them
						if (_codegenType.HasSubscriptOverload) 
						{
							baseClassName = "Array";
							break;
						}
					}
#pragma warning disable CS8604 // TPC: this can't ever be null here - but the C# code analysis believes it can - so we suppress that warning here
					baseClassName = _typeTranslator.GetTSTypeFullName(baseClass);
#pragma warning restore CS8604 // Possible null reference argument.
					break;
				}
				baseClass = baseClass.BaseType;
			}
			return baseClassName;
		}

		// TPC: this is laughably overly specific - we'll need to come up with a better way of determining whether a class should have the index/subscript operator
		private void AddIndexOperator()
		{
			if (_codegenType.IsIReadOnlyList)
			{
				var type = _codegenType.Type;
				var genericArgs = type.GetGenericArguments();
				if ((genericArgs != null) && (genericArgs.Length > 0))
				{
					var firstGenericArg = genericArgs[0];
					var genericArgName = firstGenericArg.Name;
					_typescriptCodeFormatter.AppendLine($"[index: number]: {genericArgName};");

				}
			}
		}

		private void AppendClass()
		{
			var type = _codegenType.Type;
			string typeDeclaration = string.Empty; // exp: class, abstract class or interface (https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/language-specification/namespaces#137-type-declarations)
			string inheritanceKeyword = "implements";
			if (type.IsInterface)
			{
				typeDeclaration = "interface";
				inheritanceKeyword = "extends";
			}
			else
			{
				if (type.IsAbstract)
				{
					typeDeclaration = "abstract ";
				}
				typeDeclaration += "class";
			}
			var baseClassName = GetBaseClassName(type);
			if ((type.IsInterface) && (!string.IsNullOrEmpty(baseClassName))){
				throw new System.Exception("TypescriptDeclarationGenerator: AppendClass: class: " + type.FullName + " is both an interface AND extends from base class: " + baseClassName + " our codegen does not know how to do deal with that!");
			}
			var interfaces = _codegenType.GetImplementInterfacesString();
			var extends = string.IsNullOrEmpty(baseClassName) ? "" : $" extends {baseClassName}";
			var implements = string.IsNullOrEmpty(interfaces) ? "" : $" {inheritanceKeyword} {interfaces}";
			var typeName = _typeTranslator.GetTSTypeFullName(type, false, true);
			_typescriptCodeFormatter.Append($"{typeDeclaration} {typeName}{extends}{implements} ");
			_typescriptCodeFormatter.AppendBracket();

			AddIndexOperator();

			AppendFields(_codegenType.FieldInfos);
			AppendProperties(_codegenType.PropertyInfos);

			// probably need to handle events:
			AppendEvents(_codegenType.EventInfos);

			AppendMethods(_codegenType.ConstructorInfos);

			AppendMethods(_codegenType.MethodInfos);

			AppendExtensionMethods(_codegenType.ExtensionMethods);

			AppendDerivedExtensionMethods(_codegenType.DerivedExtensionMethods);
		}

		private void AppendEnum()
		{
			var namespaceName = _codegenType.Type.Namespace;
			var prefix = "";
			if (string.IsNullOrEmpty(namespaceName))
			{
				prefix = "declare ";
			}
			_typescriptCodeFormatter.Append("{0}enum {1} ", prefix, _codegenType.Type.Name);
			_typescriptCodeFormatter.AppendBracket();
			foreach (var ev in Enum.GetValues(_codegenType.Type))
			{
				var name = Enum.GetName(_codegenType.Type, ev);
				var enumValue = Convert.ToInt32(ev);
				_typescriptCodeFormatter.AppendLine($"{name} = {enumValue},");
			}
		}

		private void AppendClassOrEnum()
		{
			if (_codegenType.Type.IsEnum)
			{
				AppendEnum();
			}
			else
			{
				AppendClass();
			}
		}

		public void GenerateTypescriptDefinition()
		{
			if (_generatedDefinitions)
			{
				throw new System.Exception("TypescriptDeclarationGenerator: no!  bad!  you don't call GenerateTypescriptDefinition twice on the same instance!  Type: " + _codegenType.Type);
			}
			_generatedDefinitions = true;

			AppendFilePrefix();
			AppendNamespace();
			AppendClassOrEnum();
		}

		public override string ToString()
		{
			return _typescriptCodeFormatter.ToString();
		}
	}

}
